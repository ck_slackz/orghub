# orghub

A community organizing tool for managing contacts, sending alerts, publishing events, etc

## Setup

* install jdk (version 8 recommended)
* install [postgresql](https://www.postgresql.org/download/)
* install latest version of [leiningen](https://leiningen.org/)
* install [direnv](https://github.com/direnv/direnv#install)
* create db user for project:
  - `createuser -P orghub && createdb -O orghub orghub`
  - enter password for user (migration config defaults to "orghub" for local dev)
* Inside repository directory
  * run database migrations:
    - `lein clj-sql-up migrate`
  * load in necessary environment variables
    * `cp .envrc.example .envrc && direnv allow`
* start server and navigate to [localhost:3449](http://localhost:3449/).
  - `lein figwheel`

## TODOs
* list views for groups, contacts, etc
* add db encryption for sensitive data
* automate development setup process
  * docker?
* wire up IDB in between server api calls and front-end
* add more items to this list :)

## License

Copyright © 2018 Christopher Kuttruff

The source code for this project (all code existing within the `src/` and `migrations/` subdirectories) is licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html) (see LICENSE.txt).  All other dependencies and linked libraries are under their respective licenses
